import React from 'react';
import { StatusBar, Button, ActivityIndicator, FlatList, StyleSheet, View } from 'react-native';
import { connect } from 'unistore/react';
import ListItem from './ListItem';

const actions = store => ({
  async getNews() {
    let res = await fetch('https://api.reddit.com/r/pics/new.json');
    var news = await res.json();
    store.setState({ response: news.data.children });
  },
  getNewsBeging() {
    store.setState({ response: [] });
  },
});

class List extends React.Component {
  async componentDidMount() {
    const { getNews } = this.props;
    await getNews();
  }
  keyExtractor = item => item.data.id;
  renderItem = ({ item }) => <ListItem item={item} />;

  reloadList = async () => {
    const { getNewsBeging, getNews } = this.props;

    getNewsBeging();

    await getNews();
  };
  render() {
    const { response } = this.props;

    return (
      <View>
        <StatusBar hidden />
        <Button
          title="Reload me"
          color="#841584"
          onPress={this.reloadList}
          accessibilityLabel="Reload the new when clicking me"
        />
        {response.length === 0 ? (
          <ActivityIndicator size="large" color="green" />
        ) : (
          <FlatList data={response} keyExtractor={this.keyExtractor} renderItem={this.renderItem} />
        )}
      </View>
    );
  }
}

export default connect(
  'response',
  actions
)(List);
