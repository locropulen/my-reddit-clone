import React from 'react';
import renderer from 'react-test-renderer';

import createStore from 'unistore';

import { Provider } from 'unistore/react';

import List from './List';
let store = createStore({ response: [] });

describe('<List />', () => {
  it('hsould match snapshot', () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <List />
      </Provider>
    );
    expect(wrapper.toJSON()).toMatchSnapshot();
  });
});
