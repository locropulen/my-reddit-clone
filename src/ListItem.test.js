import React from 'react';
import renderer from 'react-test-renderer';

import ListItem from './ListItem';

describe('<ListItem />', () => {
  it('has 2 child', () => {
    const item = { data: { created_utc: '' } };
    const tree = renderer.create(<ListItem item={item} />).toJSON();
    expect(tree.children.length).toBe(2);
  });
});
