import React from 'react';
import { StyleSheet, TouchableOpacity, Linking, Image, Text, View } from 'react-native';
import moment from 'moment';

class ListItem extends React.Component {
  handleClick = () => {
    Linking.openURL(this.props.item.data.url);
  };
  render() {
    const { item } = this.props;

    const { data } = item;

    var stillUtc = moment.utc(item.created_utc).toDate();
    var local = moment(stillUtc)
      .local()
      .format('DD-MM-YYYY HH:mm');

    return (
      <TouchableOpacity style={styles.container} onPress={this.handleClick}>
        <Image
          style={{
            width: data.thumbnail_width,
            height: data.thumbnail_height,
          }}
          source={{
            uri: data.thumbnail,
          }}
        />
        <View style={styles.content}>
          <View style={styles.date}>
            <Text style={styles.dateText}>{local}</Text>
          </View>

          <View style={styles.listItem}>
            <Text>{data.title}</Text>
            <View style={styles.footer}>
              <Text>{data.author}</Text>
              <Text>{data.score}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default ListItem;

const styles = StyleSheet.create({
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'cyan',
  },
  listItem: {
    margin: 5,
  },
  dateText: {
    color: 'white',
  },
  date: {
    margin: 5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: 'magenta',
  },
  content: {
    flex: 1,
    backgroundColor: 'yellow',
    justifyContent: 'space-between',
  },
  container: {
    margin: 5,
    padding: 5,
    borderColor: '#333',
    borderWidth: 1,
    backgroundColor: '#ccc',
    flexDirection: 'row',
    flex: 1,
  },
});
