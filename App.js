import React from 'react';

import createStore from 'unistore';

import { Provider } from 'unistore/react';

import List from './src/List';

let store = createStore({ response: [] });

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <List />
      </Provider>
    );
  }
}
